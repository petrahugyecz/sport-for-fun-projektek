# Sport For Fun projektek

<img src="https://gitlab.com/petrahugyecz/sport-for-fun-projektek/uploads/c00a255738b3a9e23ff0f1e66ac2f8b2/logo_cut.png" width="150" height="130" />

A **Sport For Fun** pálya egy teljesen új megközelítésű, Sporttainment aktivitás, amely a kézilabdázás élményszerű bemutatására jött létre.

A pálya látogatói a kézilabda-sport elemeit gyakorolhatják különböző állomásokon. Az állomásokat a legmodernebb mérőeszközök segítik és szemléltetik, így a mozgás egyszerre digitális élménnyé is válik. Az eredményeket személyre szabott módon rögzítjük, így mindenkiről kiderülhet, melyik szegmensben tud átlagon felülit nyújtani.

A Sport For Fun célja, hogy legfiatalabb korosztálytól kezdve az igazi kézilabda-rajongókig mindenkivel megismertesse és megszerettesse a kézilabdázást.

Együttműködésünk az alábbi projektekben:
- memóriajáték 🎲
- hangerősség-mérő 🔊
- több fajta egyensúlyozással irányítható játék 🤹
- ranglista 📈


🤾 Honlap: https://sportforfun.hu/hu/ 
